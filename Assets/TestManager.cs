﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManager : MonoBehaviour
{
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    Stop();
        //}
        //else if (Input.GetKeyDown(KeyCode.P)) {
        //    Play();
        //}
    }

    public void Stop() {
        GameObject[] videos = GameObject.FindGameObjectsWithTag("Video");
        for (int i = 0; i < videos.Length; i++) {
            Debug.Log(videos[i].name);
            Test1 test1 = videos[i].GetComponent<Test1>();
            test1.Stop();
        }
    }

    public void Play()
    {
        GameObject[] videos = GameObject.FindGameObjectsWithTag("Video");
        for (int i = 0; i < videos.Length; i++)
        {
            Debug.Log(videos[i].name);
            Test1 test1 = videos[i].GetComponent<Test1>();
            test1.Play();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        Stop();
    }


}
